/*
 * FreeRTOS Kernel V10.5.1
 * Copyright (C) 2021 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * SPDX-License-Identifier: MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * https://www.FreeRTOS.org
 * https://github.com/FreeRTOS
 *
 */

/*-----------------------------------------------------------
 * Implementation of functions defined in portable.h for the RISC-V RV32 port.
 *----------------------------------------------------------*/

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "portmacro.h"
#include "core_riscv.h"
/* Standard includes. */
#include "string.h"


#ifndef configKERNEL_INTERRUPT_PRIORITY
    #define configKERNEL_INTERRUPT_PRIORITY    15
#endif

/* Let the user override the pre-loading of the initial LR with the address of
prvTaskExitError() in case it messes up unwinding of the stack in the
debugger. */
#ifdef configTASK_RETURN_ADDRESS
	#define portTASK_RETURN_ADDRESS	configTASK_RETURN_ADDRESS
#else
	//#define portTASK_RETURN_ADDRESS	prvTaskExitError
    #define portTASK_RETURN_ADDRESS 0
#endif

/* The stack used by interrupt service routines.  Set configISR_STACK_SIZE_WORDS
to use a statically allocated array as the interrupt stack.  Alternative leave
configISR_STACK_SIZE_WORDS undefined and update the linker script so that a
linker variable names __freertos_irq_stack_top has the same value as the top
of the stack used by main.  Using the linker script method will repurpose the
stack that was used by main before the scheduler was started for use as the
interrupt stack after the scheduler has started. */
#ifdef configISR_STACK_SIZE_WORDS
	static __attribute__ ((aligned(16))) StackType_t xISRStack[ configISR_STACK_SIZE_WORDS ] = { 0 };
	const StackType_t xISRStackTop = ( StackType_t ) &( xISRStack[ configISR_STACK_SIZE_WORDS & ~portBYTE_ALIGNMENT_MASK ] );

	/* Don't use 0xa5 as the stack fill bytes as that is used by the kernerl for
	the task stacks, and so will legitimately appear in many positions within
	the ISR stack. */
	#define portISR_STACK_FILL_BYTE	0xee
#else
	/* __freertos_irq_stack_top define by .ld file */
	extern const uint32_t __freertos_irq_stack_top[];
	const StackType_t xISRStackTop = ( StackType_t ) __freertos_irq_stack_top;
#endif


/* Holds the critical nesting value - deliberately non-zero at start up to
 * ensure interrupts are not accidentally enabled before the scheduler starts. */
size_t  xCriticalNesting = 0xaaaaaaaa;

/* Used to catch tasks that attempt to return from their implementing function. */
size_t xTaskReturnAddress = ( size_t ) portTASK_RETURN_ADDRESS;


/*-----------------------------------------------------------*/

/*
 * Unlike other ports pxPortInitialiseStack() is written in assembly code as it
 * needs access to the portasmADDITIONAL_CONTEXT_SIZE constant.  The prototype
 * for the function is as per the other ports:
 * StackType_t *pxPortInitialiseStack( StackType_t *pxTopOfStack, TaskFunction_t pxCode, void *pvParameters );
 *
 * As per the standard RISC-V ABI pxTopcOfStack is passed in in a0, pxCode in
 * a1, and pvParameters in a2.  The new top of stack is passed out in a0.
 *
 * RISC-V maps registers to ABI names as follows (X1 to X31 integer registers
 * for the 'I' profile, X1 to X15 for the 'E' profile, currently I assumed).
 *
 * Register     ABI Name    Description                     Saver
 * x0           zero        Hard-wired zero                 -
 * x1           ra          Return address                  Caller
 * x2           sp          Stack pointer                   Callee
 * x3           gp          Global pointer                  -
 * x4           tp          Thread pointer                  -
 * x5-7         t0-2        Temporaries                     Caller
 * x8           s0/fp       Saved register/Frame pointer    Callee
 * x9           s1          Saved register                  Callee
 * x10-11       a0-1        Function Arguments/return values Caller
 * x12-17       a2-7        Function arguments              Caller
 * x18-27       s2-11       Saved registers                 Callee
 * x28-31       t3-6        Temporaries                     Caller
 *
 * The RISC-V context is saved t FreeRTOS tasks in the following stack frame,
 * where the global and thread pointers are currently assumed to be constant so
 * are not saved:
 * x31
 * x30
 * x29
 * x28
 * x27
 * x26
 * x25
 * x24
 * x23
 * x22
 * x21
 * x20
 * x19
 * x18
 * x17
 * x16
 * x15
 * x14
 * x13
 * x12
 * x11
 * pvParameters
 * x9
 * x8
 * x7
 * x6
 * x5
 * portTASK_RETURN_ADDRESS
 * [chip specific registers go here]
 * pxCode
 */

StackType_t * pxPortInitialiseStack( StackType_t * pxTopOfStack,
                                     TaskFunction_t pxCode,
                                     void * pvParameters )
{
    /* Simulate the stack frame as it would be created by a context switch interrupt. */
    pxTopOfStack -= 22;                             /* Space for registers x11-x31. */
    *pxTopOfStack = ( StackType_t ) pvParameters;   /* Task parameters (pvParameters parameter) goes into register X10/a0 on the stack. */
    pxTopOfStack -= 6;                              /* Space for registers x5-x9. */
    *pxTopOfStack = 0;                              /* Return address onto the stack, could be portTASK_RETURN_ADDRESS */
    pxTopOfStack--;
    *pxTopOfStack = ( StackType_t ) pxCode;         /* mret value (pxCode parameter) onto the stack. */

    return pxTopOfStack;
}
/*-----------------------------------------------------------*/

__attribute__((naked))
void SysTick_Handler(void)
{
    portcontextSAVE_CONTEXT_INTERNAL();

    SysTick->CTLR = 0;
    SYSTICK_REG_WRITE8(CNTL, 0);
    SYSTICK_REG_WRITE8(CNTH, 0);
    SysTick->CTLR = SYSTICK_CTLR_STE;

    portDISABLE_INTERRUPTS();

    /* Increment the RTOS tick. */
    if( xTaskIncrementTick() != pdFALSE )
    {
        vTaskSwitchContext();
    }

    portENABLE_INTERRUPTS();

    portcontextRESTORE_CONTEXT();
}
/*-----------------------------------------------------------*/

__attribute__((naked))
void SW_Handler(void)
{
    portcontextSAVE_CONTEXT_INTERNAL();

    portDISABLE_INTERRUPTS();

    vTaskSwitchContext();

    portENABLE_INTERRUPTS();

    portcontextRESTORE_CONTEXT();
}
/*-----------------------------------------------------------*/

__attribute__((naked))
void vPortStartFirstTask( void )
{
    /* Enable external interrupts,global interrupt is enabled at first task start. */
    NVIC_EnableIRQ(Software_IRQn);
    NVIC_EnableIRQ(SysTicK_IRQn);

    portcontextRESTORE_CONTEXT();
}
/*-----------------------------------------------------------*/


/* Set configCHECK_FOR_STACK_OVERFLOW to 3 to add ISR stack checking to task
stack checking.  A problem in the ISR stack will trigger an assert, not call the
stack overflow hook function (because the stack overflow hook is specific to a
task stack, not the ISR stack). */
#if defined( configISR_STACK_SIZE_WORDS ) && ( configCHECK_FOR_STACK_OVERFLOW > 2 )
	#warning This path not tested, or even compiled yet.

	static const uint8_t ucExpectedStackBytes[] = {
									portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE,		\
									portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE,		\
									portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE,		\
									portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE,		\
									portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE, portISR_STACK_FILL_BYTE };	\

	#define portCHECK_ISR_STACK() configASSERT( ( memcmp( ( void * ) xISRStack, ( void * ) ucExpectedStackBytes, sizeof( ucExpectedStackBytes ) ) == 0 ) )
#else
	/* Define the function away. */
	#define portCHECK_ISR_STACK()
#endif /* configCHECK_FOR_STACK_OVERFLOW > 2 */

/*-----------------------------------------------------------*/

BaseType_t xPortStartScheduler( void )
{
    extern void xPortStartFirstTask( void );

	#if( configASSERT_DEFINED == 1 )
	{
		/* Check alignment of the interrupt stack - which is the same as the
		stack that was being used by main() prior to the scheduler being
		started. */
		configASSERT( ( xISRStackTop & portBYTE_ALIGNMENT_MASK ) == 0 );

		#ifdef configISR_STACK_SIZE_WORDS
		{
			memset( ( void * ) xISRStack, portISR_STACK_FILL_BYTE, sizeof( xISRStack ) );
		}
		#endif	 /* configISR_STACK_SIZE_WORDS */
	}
	#endif /* configASSERT_DEFINED */

    /* disable hardware press stack */
    //NVIC_HaltPushCfg(DISABLE); /* done at startup */
    /* set software is lowest priority */
    NVIC_SetPriority(Software_IRQn, (configKERNEL_INTERRUPT_PRIORITY << 4));
    /* set systick is lowest priority */
    NVIC_SetPriority(SysTicK_IRQn, (configKERNEL_INTERRUPT_PRIORITY << 4));

    /* just for wch's systick,don't have mtime, V10x systick clock is HCLK/8 */
    SysTick->CTLR = 0;
    SYSTICK_REG_WRITE8(CNTL, 0);
    SYSTICK_REG_WRITE8(CNTH, 0);
    SYSTICK_REG_WRITE8(CMPLR, (configCPU_CLOCK_HZ/8/configTICK_RATE_HZ-1));
    SYSTICK_REG_WRITE8(CMPHR, 0);
    SysTick->CTLR = SYSTICK_CTLR_STE;

    /* Initialise the critical nesting count ready for the first task. */
    xCriticalNesting = 0;

    __asm volatile("ecall");

	/* Should not get here as after calling xPortStartFirstTask() only tasks
	should be executing. */
	return pdFAIL;
}
/*-----------------------------------------------------------*/

void vPortEndScheduler( void )
{
	/* Not implemented. */
	for( ;; );
}
/*-----------------------------------------------------------*/


