/*
 * FreeRTOS Kernel V10.5.1
 * Copyright (C) 2021 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * SPDX-License-Identifier: MIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * https://www.FreeRTOS.org
 * https://github.com/FreeRTOS
 *
 */


#ifndef PORTMACRO_H
#define PORTMACRO_H

#ifdef __cplusplus
extern "C" {
#endif

/*-----------------------------------------------------------
 * Port specific definitions.
 *
 * The settings in this file configure FreeRTOS correctly for the
 * given hardware and compiler.
 *
 * These settings should not be altered.
 *-----------------------------------------------------------
 */

/* Type definitions. */
#define portSTACK_TYPE	uint32_t
#define portBASE_TYPE	int32_t
#define portUBASE_TYPE	uint32_t
#define portMAX_DELAY ( TickType_t ) 0xffffffffUL
#define portWORD_SIZE 4

/* Only the standard core registers are stored by default.  Any additional
registers must be saved by the portasmSAVE_ADDITIONAL_REGISTERS and
portasmRESTORE_ADDITIONAL_REGISTERS macros - which can be defined in a chip
specific version of freertos_risc_v_chip_specific_extensions.h.  See the notes
at the top of this file. */
#define portCONTEXT_SIZE ( 29 * portWORD_SIZE )


typedef portSTACK_TYPE StackType_t;
typedef portBASE_TYPE BaseType_t;
typedef portUBASE_TYPE UBaseType_t;
typedef portUBASE_TYPE TickType_t;

/* Legacy type definitions. */
#define portCHAR		char
#define portFLOAT		float
#define portDOUBLE		double
#define portLONG		long
#define portSHORT		short

/* 32-bit tick type on a 32-bit architecture, so reads of the tick count do
not need to be guarded with a critical section. */
#define portTICK_TYPE_IS_ATOMIC 1
/*-----------------------------------------------------------*/

/* Architecture specifics. */
#ifdef __riscv64
	#error This is the RV32 port that has not yet been adapted for 64.
#endif
#define portSTACK_GROWTH			( -1 )
#define portTICK_PERIOD_MS			( ( TickType_t ) 1000 / configTICK_RATE_HZ )
#define portBYTE_ALIGNMENT			16
/*-----------------------------------------------------------*/


/* Scheduler utilities. */
extern void vTaskSwitchContext( void );
#define portYIELD()   NVIC_SetPendingIRQ(Software_IRQn)
#define portEND_SWITCHING_ISR( xSwitchRequired ) do { if( xSwitchRequired ) portYIELD(); } while( 0 )
#define portYIELD_FROM_ISR( x ) portEND_SWITCHING_ISR( x )
/*-----------------------------------------------------------*/


/* Critical section management. */
#define portDISABLE_INTERRUPTS()                  vPortRaiseBASEPRI()
#define portENABLE_INTERRUPTS()                   vPortSetBASEPRI( 0 )
#define portSET_INTERRUPT_MASK_FROM_ISR()         xPortRaiseBASEPRI()
#define portCLEAR_INTERRUPT_MASK_FROM_ISR( x )    vPortSetBASEPRI( x )
/*-----------------------------------------------------------*/

/* Architecture specific optimisations. */
#ifndef configUSE_PORT_OPTIMISED_TASK_SELECTION
	#define configUSE_PORT_OPTIMISED_TASK_SELECTION 1
#endif

#if( configUSE_PORT_OPTIMISED_TASK_SELECTION == 1 )

	/* Check the configuration. */
	#if( configMAX_PRIORITIES > 32 )
		#error configUSE_PORT_OPTIMISED_TASK_SELECTION can only be set to 1 when configMAX_PRIORITIES is less than or equal to 32.  It is very rare that a system requires more than 10 to 15 difference priorities as tasks that share a priority will time slice.
	#endif

	/* Store/clear the ready priorities in a bit map. */
	#define portRECORD_READY_PRIORITY( uxPriority, uxReadyPriorities ) ( uxReadyPriorities ) |= ( 1UL << ( uxPriority ) )
	#define portRESET_READY_PRIORITY( uxPriority, uxReadyPriorities ) ( uxReadyPriorities ) &= ~( 1UL << ( uxPriority ) )

	/*-----------------------------------------------------------*/

	#define portGET_HIGHEST_PRIORITY( uxTopPriority, uxReadyPriorities ) uxTopPriority = ( 31UL - __builtin_clz( uxReadyPriorities ) )

#endif /* configUSE_PORT_OPTIMISED_TASK_SELECTION */


/*-----------------------------------------------------------*/

/* Task function macros as described on the FreeRTOS.org WEB site.  These are
not necessary for to use this port.  They are defined so the common demo files
(which build with all the ports) will build. */
#define portTASK_FUNCTION_PROTO( vFunction, pvParameters ) void vFunction( void *pvParameters )
#define portTASK_FUNCTION( vFunction, pvParameters ) void vFunction( void *pvParameters )

/*-----------------------------------------------------------*/

#define portNOP() __asm volatile 	( " nop " )

#define portINLINE	__inline

#ifndef portFORCE_INLINE
	#define portFORCE_INLINE inline __attribute__(( always_inline))
#endif

#define portMEMORY_BARRIER() __asm volatile( "" ::: "memory" )
/*-----------------------------------------------------------*/

extern size_t xCriticalNesting;

#define portENTER_CRITICAL()           \
{                                      \
    portDISABLE_INTERRUPTS();          \
    xCriticalNesting++;                \
}

#define portEXIT_CRITICAL()            \
{                                      \
    xCriticalNesting--;                \
    if( xCriticalNesting == 0 )        \
    {                                  \
        portENABLE_INTERRUPTS();       \
    }                                  \
}

/*-----------------------------------------------------------*/

static portFORCE_INLINE void vPortSetBASEPRI( portUBASE_TYPE uBASEPRI )
{
    NVIC->ITHRESDR = uBASEPRI << 4;
}

/*-----------------------------------------------------------*/

static portFORCE_INLINE void vPortRaiseBASEPRI(void)
{
    NVIC->ITHRESDR = configMAX_SYSCALL_INTERRUPT_PRIORITY << 4;
}

/*-----------------------------------------------------------*/

static portFORCE_INLINE portUBASE_TYPE xPortRaiseBASEPRI( void )
{
    portUBASE_TYPE uReturn = NVIC->ITHRESDR >> 4;
    NVIC->ITHRESDR = configMAX_SYSCALL_INTERRUPT_PRIORITY << 4;

    return uReturn;
}

/*-----------------------------------------------------------*/

static portFORCE_INLINE void portcontextSAVE_CONTEXT_INTERNAL(void)
{
    __asm volatile (
        " addi sp, sp, -%0 \n"
        " sw x1, 1 * %1( sp ) \n"
        " sw x5, 2 * %1( sp ) \n"
        " sw x6, 3 * %1( sp ) \n"
        " sw x7, 4 * %1( sp ) \n"
        " sw x8, 5 * %1( sp ) \n"
        " sw x9, 6 * %1( sp ) \n"
        " sw x10, 7 * %1( sp ) \n"
        " sw x11, 8 * %1( sp ) \n"
        " sw x12, 9 * %1( sp ) \n"
        " sw x13, 10 * %1( sp ) \n"
        " sw x14, 11 * %1( sp ) \n"
        " sw x15, 12 * %1( sp ) \n"
        " sw x16, 13 * %1( sp ) \n"
        " sw x17, 14 * %1( sp ) \n"
        " sw x18, 15 * %1( sp ) \n"
        " sw x19, 16 * %1( sp ) \n"
        " sw x20, 17 * %1( sp ) \n"
        " sw x21, 18 * %1( sp ) \n"
        " sw x22, 19 * %1( sp ) \n"
        " sw x23, 20 * %1( sp ) \n"
        " sw x24, 21 * %1( sp ) \n"
        " sw x25, 22 * %1( sp ) \n"
        " sw x26, 23 * %1( sp ) \n"
        " sw x27, 24 * %1( sp ) \n"
        " sw x28, 25 * %1( sp ) \n"
        " sw x29, 26 * %1( sp ) \n"
        " sw x30, 27 * %1( sp ) \n"
        " sw x31, 28 * %1( sp ) \n"
        " lw  t1, pxCurrentTCB  \n"          /* Load pxCurrentTCB. */
        " sw  sp, 0( t1 ) \n"                /* Write sp to first TCB member. */

        " csrr t0, mepc \n"
        " sw t0, 0( sp ) \n"                 /* Save updated exception return address. */
        ::"i" ( portCONTEXT_SIZE ), "i" ( portWORD_SIZE )
    );
}

static portFORCE_INLINE void portcontextRESTORE_CONTEXT(void)
{
    __asm volatile (
        " lw  t1, pxCurrentTCB            \n" /* Load pxCurrentTCB. */
        " lw  sp, 0( t1 )                 \n" /* Read sp from first TCB member. */

        /* Load mret with the address of the next instruction in the task to run next. */
        " lw t0, 0( sp )                  \n"
        " csrw mepc, t0                   \n"

        " lw  x1, 1 * %1( sp )   \n"
        " lw  x5, 2 * %1( sp )   \n"   /* t0 */
        " lw  x6, 3 * %1( sp )   \n"   /* t1 */
        " lw  x7, 4 * %1( sp )   \n"   /* t2 */
        " lw  x8, 5 * %1( sp )   \n"   /* s0/fp */
        " lw  x9, 6 * %1( sp )   \n"   /* s1 */
        " lw  x10, 7 * %1( sp )  \n"   /* a0 */
        " lw  x11, 8 * %1( sp )  \n"   /* a1 */
        " lw  x12, 9 * %1( sp )  \n"   /* a2 */
        " lw  x13, 10 * %1( sp ) \n"   /* a3 */
        " lw  x14, 11 * %1( sp ) \n"   /* a4 */
        " lw  x15, 12 * %1( sp ) \n"   /* a5 */
        " lw  x16, 13 * %1( sp ) \n"   /* a6 */
        " lw  x17, 14 * %1( sp ) \n"   /* a7 */
        " lw  x18, 15 * %1( sp ) \n"   /* s2 */
        " lw  x19, 16 * %1( sp ) \n"   /* s3 */
        " lw  x20, 17 * %1( sp ) \n"   /* s4 */
        " lw  x21, 18 * %1( sp ) \n"   /* s5 */
        " lw  x22, 19 * %1( sp ) \n"   /* s6 */
        " lw  x23, 20 * %1( sp ) \n"   /* s7 */
        " lw  x24, 21 * %1( sp ) \n"   /* s8 */
        " lw  x25, 22 * %1( sp ) \n"   /* s9 */
        " lw  x26, 23 * %1( sp ) \n"   /* s10 */
        " lw  x27, 24 * %1( sp ) \n"   /* s11 */
        " lw  x28, 25 * %1( sp ) \n"   /* t3 */
        " lw  x29, 26 * %1( sp ) \n"   /* t4 */
        " lw  x30, 27 * %1( sp ) \n"   /* t5 */
        " lw  x31, 28 * %1( sp ) \n"   /* t6 */
        " addi sp, sp, %0 \n"
        " mret \n"
        ::"i" ( portCONTEXT_SIZE ), "i" ( portWORD_SIZE )
    );
}

#ifdef __cplusplus
}
#endif

#endif /* PORTMACRO_H */

