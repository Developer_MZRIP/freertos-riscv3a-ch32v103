# Порт FreeRTOS для процессора CH32V103 на ядре RISC-V3A.
### Отличия от версии из официальных примеров CH32V103EVT.ZIP:
- FreeRTOS работает в User Mode.
- Вложенные прерывания приоритетом равным configMAX_SYSCALL_INTERRUPT_PRIORITY и ниже не блокируются ядром.
> Схема вложенности прерываний RTOS делит все доступные приоритеты на 2 группы – те, которые будут маскироваться критическими секциями RTOS, и те, которые не маскируются          критическими секциями, и поэтому всегда разрешены. Настройка configMAX_SYSCALL_INTERRUPT_PRIORITY в FreeRTOSConfig.h определяет границу между этими двумя группами.
> Функции FreeRTOS, которые заканчиваются на "FromISR", являются безопасными для вызова из обработчика прерывания (interrupt safe), но даже эти функции не могут быть вызваны из прерываний, у которых логический приоритет (не числовой!) выше, чем приоритет, определенный макросом configMAX_SYSCALL_INTERRUPT_PRIORITY (configMAX_SYSCALL_INTERRUPT_PRIORITY определен в файле заголовка FreeRTOSConfig.h). Таким образом, любой ISR, который использует функцию RTOS API, должен быть с установленным вручную приоритетом, на значение, которое как число равно или больше чем configMAX_SYSCALL_INTERRUPT_PRIORITY. Это гарантирует, что логический приоритет этого ISR равен или меньше логической установки приоритета configMAX_SYSCALL_INTERRUPT_PRIORITY.
- Вызов первой задачи при старте планировщика реализован через ECALL.
- Стек прерываний не используется.
>  Прерывания расходуют стек задач. Для векторного режима нужно реализовать обёртку для каждого прерывания в которой переключать стек на стек прерываний и обратно.
- Аппаратный стек отключается в startup файле, до запуска main().
